<?php

use yii\db\Migration;

class m160211_143557_create_importer_data_table extends Migration
{
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_importer_data_element');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_importer_data_element', [
              'id' => $this->primaryKey(),
              'name' => $this->string(128)->notNull(),
              'raw_data' => $this->text(),
              'signature' => $this->string(128),
              'author_id' => $this->integer(),
              'imported_data_id' => $this->integer(),
              'created_at' => $this->timestamp(),
              'is_imported' => $this->boolean()->defaultValue(false),
         ]);

         //Only unique addresss
          $this->createIndex(
              'nitm_importer_data_element_index',
              'nitm_importer_data_element',
              ['name']
          );

          // add foreign key for table `user`
          $this->addForeignKey(
              'nitm_importer_data-fk',
              'nitm_importer_data_element',
              'imported_data_id',
              'nitm_importer_data',
              'id',
              'CASCADE'
          );
    }

    public function safeDown()
    {
        echo "m160211_143557_create_importer_data_table cannot be reverted.\n";

        return true;
    }
}
