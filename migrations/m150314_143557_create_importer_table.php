<?php

use yii\db\Migration;

class m150314_143557_create_importer_table extends Migration
{
    public function safeUp()
    {
        $tableSchema = \Yii::$app->db->getTableSchema('nitm_importer_data');
        if ($tableSchema) {
            return true;
        }
        $this->createTable('nitm_importer_data', [
              'id' => $this->primaryKey(),
              'title' => $this->string(128)->notNull(),
              'name' => $this->string(128)->notNull(),
              'raw_data' => $this->text(),
              'type' => $this->string(64),
              'data_type' => $this->string(64),
              'remote_id' => $this->integer(),
              'remote_type' => $this->string(64),
              'source' => $this->text(),
              'author_id' => $this->integer(),
              'integer' => $this->integer(),
              'count' => $this->integer(),
              'created_at' => $this->timestamp(),
              'completed_at' => $this->timestamp(),
              'completed_by' => $this->integer(),
              'is_imported' => $this->boolean()->defaultValue(false),
         ]);

         //Only unique addresss
          $this->createIndex(
              'nitm_importer_data_index',
              'nitm_importer_data',
              ['name', 'type', 'data_type']
          );
    }

    public function safeDown()
    {
        echo "m150314_143557_create_importer_table cannot be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
