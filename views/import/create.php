<?php
/*
 * @var yii\web\View $this
 * @var nitm\importer\models\Source $model
 */

$this->title = Yii::t('app', 'Create {modelClass}', [
  'modelClass' => 'Import Job',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Import'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="<?= $formOptions['container']['id']; ?>" class="<?= $formOptions['container']['class']?> ">

    <?= $this->render('form/_form', [
        'model' => $model,
        'formOptions' => $formOptions,
        'scenario' => $scenario,
        'action' => $action,
        'type' => $type,
        'processor' => $processor,
    ]) ?>

</div>
